# Source codes for **Drought plasticity QTLs specifically contribute to the genotype x water availability interaction in maize** Djabali *et al.*

## Description
GitLab repository where all the scripts are available to make the paper analysis and obtain the same results and figures/tables presented in Djabali *et al*., .

## Installation
```
mkdir source_code_article
cd source_code_article
git init
git clone https://forgemia.inra.fr/yacine.djabali/drought-plasticity-qtls-article.git
```
## Repository content
### Scripts 
A folder containing scripts that gives all the elements of the article (e.g. figures, tables, supplementary figures and tables).

### R_inputs
Folder containing necessary data to make outputs with the scripts. The different outputs are organized into three sub-folders :
- GWAS_results: Results obtained after genome-wide association studies
- Previous_study: Data provided by previous publications
- formatted_data: Pre-processed raw data to facilitate analyses

### R_outputs
 Folder containing two sub-folders : 
 - Figures: Folder where all figures are saved
 - Tables: Folder where all tables and data are saved
### GWAS
Folder about genome-wide association studies. It follows the same folder architecture as the repository's root.
 
## Usage
After installation of the repository, all scripts allowing analyses related to the article are provided in ./Scripts.

Since genome-wide association studies are time-consuming, all results are presented in the ./R_inputs/GWAS_results folder to make analyses without time constraints.
If you want to make GWAS, you can find a dedicated folder in the root of this repository ./GWAS. 

## Support
For any questions about this work, please contact :
- Yacine Djabali (Author of the repository): djabaliyacine@yahoo.fr
-  Mélisande Blein-Nicolas (The corresponding author) : melisande.blein-nicolas@universite-paris-saclay.fr
