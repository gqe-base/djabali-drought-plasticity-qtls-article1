# Sampling for Steady state union random QTLs set

# 0. Description  ------------------------------------------------

# Inputs : steady state QTLs, plasticity QTLs, .
#          Traits WD and WW adjusted means obtained in the 4 experiments
#          Genotyping data to estimate kinship matrix with statgenGWAS package (kinship function)


# 1.  Path & Libraries --------------------------------------------------------------


# 1.1. Path to working directory ---------------------------------------------------------------
## variable to modify##
git_path <- "~/git/"

# 1.2. Libraries  --------------------------------------------------

library(stringr)
library(lme4)
library(statgenGWAS)
library(tidyr)
library(dplyr)
library(lme4)
library(factoextra)
library(FactoMineR)
set.seed(31011997)
args = commandArgs(TRUE)
args = as.character(args)
# 1.3. Path to inputs ---------------------------------------------

# 1.3.1. Traits adjusted means  -------------------------------------------

WD <- readRDS(paste0(git_path,"drought-plasticity-qtls-article/R_inputs/Previous_study/WD_254x24var_envbyenv_Alvarez_Prado_et_al_2017.rds"))
WW <- readRDS(paste0(git_path,"drought-plasticity-qtls-article/R_inputs/Previous_study/WW_254x24var_envbyenv_Alvarez_Prado_et_al_2017.rds"))

# 1.3.2. QTLs ------------------------------------------------------------

QTL_steady_state_WW <- readRDS(file = paste0(git_path,"drought-plasticity-qtls-article/R_inputs/GWAS_results/Multi_env_QTL_ww.rds"))
QTL_steady_state_WD <- readRDS(file = paste0(git_path,"drought-plasticity-qtls-article/R_inputs/GWAS_results/Multi_env_QTL_wd.rds"))
QTL_plasticity <- readRDS(file = paste0(git_path,"drought-plasticity-qtls-article/R_inputs/GWAS_results/Multi_env_QTL_plasticity.rds"))

# 1.3.3. Genotyping data -------------------------------------------------------

load(paste0(git_path,"drought-plasticity-qtls-article/R_inputs/Previous_study/Genotyping_matrix_Negro_et_al_2019.Rdata"))

# 2. Functions  --------------------------------------------------------------


# 3. Main -----------------------------------------------------------------

# 3.1. Tables formatting -----------------------------------------------

WD$Treatment <- as.factor("WD")
WW$Treatment <- as.factor("WW")

all_data <- rbind(WD,WW)

Lal <- all_data %>% dplyr::select(contains("variety") | contains("Lal")| contains("Treatment"))
Biol <- all_data %>% dplyr::select(contains("variety")|contains("Biol")| contains("Treatment"))
WU <- all_data %>% dplyr::select((contains("variety")|contains("WU")| contains("Treatment")) & !contains("WUE"))
WUE <- all_data %>% dplyr::select(contains("variety")|contains("WUE")| contains("Treatment"))
Trate <- all_data %>% dplyr::select(contains("variety")|contains("Trate")| contains("Treatment"))
gs_max <- all_data %>% dplyr::select(contains("variety")|contains("gs_max")| contains("Treatment"))

colnames(Lal)[2:5] <- sapply(colnames(Lal)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])
colnames(Biol)[2:5] <- sapply(colnames(Biol)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])
colnames(WU)[2:5] <- sapply(colnames(WU)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])
colnames(WUE)[2:5] <- sapply(colnames(WUE)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])
colnames(Trate)[2:5] <- sapply(colnames(Trate)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])
colnames(gs_max)[2:5] <- sapply(colnames(gs_max)[2:5], function(x) unlist(strsplit(x,"[.]"))[1])


Lal <- gather(data = Lal, key = S,value = Value, 2:5, factor_key=TRUE)
Biol <- gather(data = Biol, key = S,value = Value,2:5, factor_key=TRUE)
WU <- gather(data = WU, key = S,value = Value, 2:5, factor_key=TRUE)
WUE <- gather(data = WUE, key = S,value = Value, 2:5, factor_key=TRUE)
Trate <- gather(data = Trate, key = S,value = Value, 2:5, factor_key=TRUE)
gs_max <- gather(data = gs_max, key = S,value = Value, 2:5, factor_key=TRUE)


all_trait <-list(Lal,Biol,WU,WUE,Trate,gs_max)
names(all_trait) <- c("LAl","Biol","WU","WUE","Trate","gs_max")

colnames(mat2) <- str_replace_all(colnames(mat2),"-","_")
QTL_steady_state_WW$SNP.name <- str_replace(QTL_steady_state_WW$SNP.name,"-","_")
QTL_steady_state_WD$SNP.name <- str_replace(QTL_steady_state_WD$SNP.name,"-","_")
QTL_plasticity$SNP.name <- str_replace(QTL_plasticity$SNP.name,"-","_")

res <- NULL

# 3.2. Kinship matrix estimation and ACP axes extraction  -----------------------------------------------

mat2 <- mat2[WD$variety,]
kin <- kinship(mat2,method = "astle")
res.pca <- PCA(kin, scale.unit = T,graph = FALSE,ncp = 254)
nb_dim <- as.numeric(unlist(strsplit(names(res.pca$eig[which(res.pca$eig[,1]>1),3])[length(res.pca$eig[which(res.pca$eig[,1]>1),3])]," "))[2])

PC <- res.pca$ind$coord[,1:nb_dim]
colnames(PC) <- sapply(1:nb_dim,function(x) paste0("PC",x))
PC <- data.frame(PC)
PC$variety <- rownames(PC)


# 3.3. Variances components estimation for each traits -----------------------------------------------

for(i in  1:1000){
  tmp <- all_trait[[args]]
  tmp <- merge(tmp,PC,by.x = "variety",by.y = "variety")
  tmp$Env <- as.factor(paste0(as.character(tmp$S),".",as.character(tmp$Treatment)))
  tmp$Exp <- tmp$S
  
  
  QTLs_all <- kinship(mat2[,unique(c(sample(colnames(mat2)[-1],length(QTL_plasticity$SNP.name[which(QTL_plasticity$Trait==args)])),QTL_steady_state_WD$SNP.name[which(QTL_steady_state_WD$Trait==args)],QTL_steady_state_WW$SNP.name[which(QTL_steady_state_WW$Trait==args)]))],method = "astle")
  res.QTLs_all <- PCA(QTLs_all, scale.unit = T,graph = FALSE,ncp = 254)
  nb_dim_QTLs_all <- as.numeric(unlist(strsplit(names(res.QTLs_all$eig[which(res.QTLs_all$eig[,1]>1),3])[length(res.QTLs_all$eig[which(res.QTLs_all$eig[,1]>1),3])]," "))[2])
  QTLs_all <- res.QTLs_all$ind$coord[,1:nb_dim_QTLs_all]
  if(nb_dim_QTLs_all>1){
    colnames(QTLs_all) <- sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x))
    QTLs_all <- data.frame(QTLs_all)
  }else{
    QTLs_all <- data.frame(Q1 = QTLs_all)
  }
  QTLs_all$variety <- rownames(QTLs_all)
  tmp_QTLs_all <- merge(tmp,QTLs_all,by.x = "variety",by.y = "variety")
  
  colnames(tmp_QTLs_all)[1] <- "G"
  tmp_QTLs_all$G <- as.factor(tmp_QTLs_all$G)
  
  
  if(args == "LAl"){
  
    formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) + (1|G:Exp)")
    formu <- as.formula(formu)
    mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
    var_QTL_all <- unlist(VarCorr(mod_QTL_all))
    res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
    res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
    
    res <- rbind(res,res_QTL_all)
    
    
  }else{
    if(args == "Biol"){
      
      formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) + (1|G:Exp)")
      formu <- as.formula(formu)
      mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
      var_QTL_all <- unlist(VarCorr(mod_QTL_all))
      res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
      res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
      
      res <- rbind(res,res_QTL_all)
      
    }else{
      if(args == "Trate"){

        formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) + (1|G:Exp)")
        formu <- as.formula(formu)
        mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
        var_QTL_all <- unlist(VarCorr(mod_QTL_all))
        res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
        res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
        
        res <- rbind(res,res_QTL_all)
        
      }else{
        if(args == "gs_max"){
          
          formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) + (1|G:Exp)")
          formu <- as.formula(formu)
          mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
          var_QTL_all <- unlist(VarCorr(mod_QTL_all))
          res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
          res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
          
          res <- rbind(res,res_QTL_all)
          
        }else{
          if(args == "WU"){

            formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) + (1|G:Exp)")
            formu <- as.formula(formu)
            mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
            var_QTL_all <- unlist(VarCorr(mod_QTL_all))
            res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
            res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
            
            res <- rbind(res,res_QTL_all)
            
          }else{
            if(args == "WUE"){

              formu <- paste0("Value ~ Env + ",paste(sapply(1:nb_dim,function(x) paste0("PC",x," + PC",x,":Env")),collapse = "+")," + ",paste(sapply(1:nb_dim_QTLs_all,function(x) paste0("Q",x," + Q",x,":Env")),collapse = "+")," + (1|G) + (1|G:Treatment) +  (1|G:Exp)")
              formu <- as.formula(formu)
              mod_QTL_all <-  lmer(formu,data=tmp_QTLs_all)
              var_QTL_all <- unlist(VarCorr(mod_QTL_all))
              res_QTL_all <- c("Steady state union plasticity",args,var_QTL_all,attr(VarCorr(mod_QTL_all), "sc")^2)
              res_QTL_all <- c(res_QTL_all[1:2],res_QTL_all[5],res_QTL_all[4],res_QTL_all[3],res_QTL_all[5])
              
              res <- rbind(res,res_QTL_all)
              
            }
          }
        }
      }
    }
  }
  
  print(i)
}
res <- data.frame(res)
colnames(res) <- c("Model","Trait","G","GxW","GxF","Residual error")



# 4. Saving outputs --------------------------------------------
saveRDS(res,paste0(git_path,"drought-plasticity-qtls-article/R_outputs/Tables/variances_steady_state_QTLs_union_random_QTLs_",args,".rds"))

